import os
import json
import random
import unittest
from shutil import rmtree
from PIL import Image
from app.project_creator import BASE_DIR, ProjectCreator
from app.manager import ManagerEmitter


class ProjectCreatorTest(unittest.TestCase):
    """
    Class that contains test of ProjectCreatorTest.
    """
    def setUp(self):
        self.data = {
            "image_list": self.get_image_list(),
            "project_data": {
                "path": os.path.join(BASE_DIR, "projects"),
                "template": random.choice(self.get_list_dir("templates")),
                "title": "Our journey to Brazil - June/July 2013",
                "description": "#rio #saopaulo #christstatue #maracana"
            },
            "emitter": ManagerEmitter()
        }

    def tearDown(self):
        for p in self.get_list_dir("projects"):
            rmtree(os.path.join(BASE_DIR, "projects", p))

    def get_image_list(self):
        """
        Returns list with dictionaries that contain info about images
        """
        path = BASE_DIR + "/assets/"
        result = []

        for im in self.get_list_dir("assets"):
            im_dict = {"path": path + im}
            im_dict["name"], im_dict["ext"] = im_dict["path"].split("/")[-1].split(".")
            result.append(im_dict)

        return result

    def get_list_dir(self, directory):
        """
        Returns listdir of passed directory

        Argument/s:
            directory - directory to list it
        """
        return os.listdir(BASE_DIR + "/" + directory)

    def prepare_images(self, creator):
        """
        Invokes method for preparing images.
        """
        for index in range(len(self.data["image_list"])):
            creator.prepare_image(self.data["image_list"][index], index)

    def test_copy_template_structure_method(self):
        """
        Tests project creator copy template method.
        Structure of template should be copied properly.
        """
        c = ProjectCreator(**self.data)
        c.copy_template_structure()

        copied_path = lambda x: os.listdir(c.project_path + x)

        self.assertEqual(["assets"], copied_path(""))
        self.assertEqual(["style.css"], copied_path("/assets/css"))
        self.assertEqual(["app-min.js", "app.js"], copied_path("/assets/js"))
        self.assertEqual(
            set(['img', 'fonts', 'css', 'js']), set(copied_path("/assets"))
        )

    def test_copy_template_structure_method_with_error(self):
        """
        Tests project creator copy template method.
        Exception that project already exists should be raised.
        """
        c = ProjectCreator(**self.data)
        c.copy_template_structure()

        self.assertRaises(FileExistsError, lambda: c.copy_template_structure())

    def test_prepare_image_method(self):
        """
        Tests project creator prepare image method.
        Images and thumbs of them should be created.
        """
        c = ProjectCreator(**self.data)
        c.copy_template_structure()

        self.prepare_images(c)

        created_images = os.listdir(c.project_path + "assets/img")
        assets_path = "assets/img/"

        for index in range(len(self.data["image_list"])):
            normal_file = "{0}.png".format(index)
            thumb_file = "{0}_thumb.png".format(index)

            self.assertIn(normal_file, created_images)
            self.assertIn(thumb_file, created_images)

            expected_image_dict = {
                "normal": assets_path + normal_file,
                "thumb": assets_path + thumb_file
            }

            self.assertEqual(expected_image_dict, c.converted_list[index])

    def test_prepare_image_method_with_error(self):
        """
        Tests project creator prepare image method.
        Exception that file doesn't exist should be raised.
        """
        self.data["image_list"].append(
            {
                "path": os.getcwd() + "/idonotexist.jpg",
                "name": "idonotexist",
                "ext": "jpg"
            }
        )

        c = ProjectCreator(**self.data)
        c.copy_template_structure()

        self.assertRaises(FileNotFoundError, lambda: self.prepare_images(c))

    def test_build_template_method(self):
        """
        Tests project creator build template method.
        Template with proper title and description should be created.
        """
        c = ProjectCreator(**self.data)
        c.build_project()

        images = json.dumps(c.converted_list)

        with open(c.project_path + "/index.html", mode="r") as f:
            content = f.read()

            self.assertIn(images, content)

            for value in self.data["project_data"].values():
                self.assertIn(str(value), content)

    def test_get_thumb_size_method(self):
        """
        Tests project creator get thumb size method.
        Proper tuple with size should be returned.
        """
        path = BASE_DIR + '/assets/' + random.choice(self.get_list_dir("assets"))

        with Image.open(path) as im:
            self.assertEqual(ProjectCreator.get_thumb_size(im), (500, 280))

    def test_remove_project_method(self):
        """
        Tests project creator remove project method.
        Created project should be removed.
        """
        c = ProjectCreator(**self.data)
        c.build_project()

        self.assertGreater(len(self.get_list_dir("projects")), 0)
        c.remove_project()
        self.assertEqual(len(self.get_list_dir("projects")), 0)


if __name__ == "__main__":
    unittest.main()
