import sys
from PyQt5 import QtWidgets, QtGui, QtCore
from app.manager import ImageManagerWidget
from app.settings import SettingsWidget
from app.database import DatabaseHandler


class MainWindow(QtWidgets.QMainWindow):
    """
    Main window of whole app.

    Attribute/s:
        central - instance of CentralWidget
    """
    def __init__(self, geometry, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.central = CentralWidget(parent=self, geometry=geometry)

        self.setWindowTitle("AlbumGEN")
        self.setCentralWidget(self.central)

        self.init_project_menu()
        self.init_settings_menu()

    def init_project_menu(self):
        """
        Creates menu with project menu that contains all actions related with
        creating, loading, deleting and saving project and adds it to main menu
        bar.
        """
        proj_menu = self.menuBar().addMenu("&Projekt")

        # Invokes function responsible for restarting info about current project
        new_proj = QtWidgets.QAction("Nowy projekt", self)
        new_proj.setShortcut("CTRL+N")
        new_proj.triggered.connect(
            self.central.tab_widget.image_manager.start_new_project
        )
        proj_menu.addAction(new_proj)

        # Anonymous function that changes index of current tab of tab widget to
        # second (instance of SettingsWidget)
        change_tab = lambda: self.central.tab_widget.tab.setCurrentIndex(1)

        # Actions redirects to tab with widget that contains list with projects
        # that can be loaded from there
        load_proj = QtWidgets.QAction("Załaduj projekt", self)
        load_proj.setShortcut("CTRL+L")
        load_proj.triggered.connect(change_tab)
        proj_menu.addAction(load_proj)

        # Action redirects to tab with widget that contains list with projects
        # that can be deleted from there
        del_proj = QtWidgets.QAction("Usuń projekt", self)
        del_proj.setShortcut("CTRL+D")
        del_proj.triggered.connect(change_tab)
        proj_menu.addAction(del_proj)

        # Action invokes function responsible for save or update project
        save_proj = QtWidgets.QAction("Zapisz projekt", self)
        save_proj.setShortcut("CTRL+S")
        save_proj.triggered.connect(
            self.central.tab_widget.image_manager.modify_project
        )
        proj_menu.addAction(save_proj)

    def init_settings_menu(self):
        """
        Creates menu with settings menu that contains all actions related with
        setting project's dictionary and language (in future).
        """
        set_menu = self.menuBar().addMenu("&Ustawienia")

        # Action invokes function responsible for changing project's directory
        ch_dir = QtWidgets.QAction("Zmień lokalizację projektów", self)
        ch_dir.setShortcut("CTRL+SHIFT+C")
        ch_dir.triggered.connect(
            self.central.tab_widget.settings.change_directory
        )
        set_menu.addAction(ch_dir)


class CentralWidget(QtWidgets.QWidget):
    """
    Widget that is considered as central.
    Works as wrapper for header and tab widget.

    Attribute/s:
        header_widget - instance of HeaderWidget
        tab_widget - instance of TabWidget
    """
    def __init__(self, geometry, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.header_widget = HeaderWidget(parent=self)
        self.tab_widget = TabWidget(parent=self, geometry=geometry)

        vbox = QtWidgets.QVBoxLayout()
        vbox.addWidget(self.header_widget)
        vbox.addWidget(self.tab_widget)

        self.setLayout(vbox)


class HeaderWidget(QtWidgets.QWidget):
    """
    Widget that contains image with header
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.setStyleSheet("background: #CDCDCD")

        logo_pixmap = QtGui.QPixmap("app/assets/logo.png")
        label = QtWidgets.QLabel(pixmap=logo_pixmap)
        label.setAlignment(QtCore.Qt.AlignCenter)

        hbox = QtWidgets.QHBoxLayout()
        hbox.addWidget(label)

        self.setLayout(hbox)


class TabWidget(QtWidgets.QWidget):
    """
    Widget that contains tab widget with image manager and settings widgets

    Attribute/s:
        database - instance of DatabaseHandler
        image_manager - instance of ImageManagerWidget
        settings - instance of SettingsWidget
        tab - instance of QTabWidget designed to store instances of
        ImageManagerWidget and SettingsWidget classes
    """
    def __init__(self, geometry, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.database = DatabaseHandler()

        self.image_manager = ImageManagerWidget(parent=self,
                                    database=self.database, geometry=geometry)

        self.settings = SettingsWidget(parent=self, database=self.database)

        self.tab = QtWidgets.QTabWidget(parent=self)
        self.tab.addTab(self.image_manager, "Kokpit")
        self.tab.addTab(self.settings, "Ustawienia")

        vbox = QtWidgets.QVBoxLayout()
        vbox.addWidget(self.tab)

        self.setLayout(vbox)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    main_window = MainWindow(geometry=app.desktop().screenGeometry())
    main_window.show()

    app.exec_()
    app.deleteLater()

    sys.exit()
