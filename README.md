# AlbumGEN

### Overview
- - -

AlbumGEN is desktop application responsible for creating photo galleries with use of predefined templates. Thanks to image preview and project managing system (saving and loading projects), generating ligthweight and good-looking galleries is fast and easy process.

App available in polish language version.
Project was completed 13th January 2017.

### Technologies
---
##### Desktop app
* Python
* PyQt5
* sqlite

##### Templates
+ HTML
+ CSS
+ SASS
+ gulp
+ Vanilla.js

### How to create a photo gallery in one minute
---
####1. Run AlbumGEN.
![Run](https://media.giphy.com/media/3tHOESk80A6jBFfIyn/giphy.gif)

####2. Set basic info about your project.
![Set](https://media.giphy.com/media/1jkV90P2JyHEYEpjcH/giphy.gif)

####3. Add photos to your project by drag'n'drop or file picker.
![Add](https://media.giphy.com/media/fxtEwamoF1owc9uYFp/giphy.gif)

####4. Check out photos that you've added to your project.
![Check](https://media.giphy.com/media/A7XHTQB2w2uyVque1M/giphy.gif)

####5. Remove undesired photos from your project.
![Remove](https://media.giphy.com/media/uTCIhTIEctNrsMEVss/giphy.gif)

####6. Choose template and generate gallery based on it.
![Generate](https://media.giphy.com/media/1Y6SLLB8DMPA6UFRue/giphy.gif)

### Templates
---
##### Click each one to see a template demo
[![Insta](https://imgur.com/J0bvasX.png)](https://media.giphy.com/media/5UI15fFdpQcyteiDSk/giphy.gif)
[![Album](https://imgur.com/YHYpkoP.png)](https://media.giphy.com/media/830Myezr96cdsSsE9m/giphy.gif)
[![Waterfall](https://imgur.com/ImcYlEj.png)](https://media.giphy.com/media/1qfe4wVh1kTJOlte2i/giphy.gif)
[![Photostack](https://imgur.com/dImAqER.png)](https://media.giphy.com/media/Nm1GN2EoKG6UD185V9/giphy.gif)
[![Mini](https://imgur.com/bzcRpbX.png)](https://media.giphy.com/media/Zd6dWPcgNRO02Oxng9/giphy.gif)
