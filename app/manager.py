import os
import re
from PyQt5 import QtWidgets, QtGui, QtCore
from .list_model import ImageListModel
from .emitters import ImageEmitter, ManagerEmitter
from .project_creator import CreatorThread, ProjectCreator, BASE_DIR
from .database import DatabaseThread
from .decorators import (
    update_image,
    MessageMixin,
    db_thread_wrapper,
    check_project_validity
)


class ImageManagerWidget(MessageMixin, QtWidgets.QWidget):
    """
    Wrapper for all widgets related with project creation

    Attribute/s:
        allowed_extensions - list with extensions of images that are allowed
        templates_names - list with names of all templates allowed to use
        database - instance of DatabaseHandler
        emitter - instance of manager emitter
        progress_dial - instance of ProgressDialog
        list_model - instance of ImageListModel
        file_widget - instance of ImageFileWidget
        image_preview - instance of ImagePreviewWidget
    """
    allowed_extensions = ['jpg', 'jpeg', 'png']
    templates_names = ["Album", "Mini", "Waterfall", "Insta", "Photostack"]

    def __init__(self, database, geometry, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.database = database

        # Configure emitter related with creating a new project
        self.emitter = ManagerEmitter()
        self.emitter.progress.connect(self.handle_progress)
        self.emitter.finished.connect(self.handle_finished)
        self.emitter.not_exist.connect(self.handle_not_exist)
        self.emitter.already_exists.connect(self.handle_already_exists)
        self.emitter.unknown_error.connect(self.handle_unknown_error)
        self.emitter.saved.connect(self.handle_saved)
        self.emitter.updated.connect(self.handle_updated)

        # Attribute reserved for progress dialog which is displayed during new
        # project creation
        self.progress_dial = None

        self.list_model = ImageListModel()
        self.setAcceptDrops(True)

        self.file_widget = ImageFileWidget(parent=self)
        self.image_preview = ImagePreviewWidget(parent=self, geometry=geometry)

        hbox = QtWidgets.QHBoxLayout()
        hbox.addWidget(self.file_widget)
        hbox.addWidget(self.image_preview)

        self.setLayout(hbox)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        #  Adding files to image list model if dropped file is an image with
        #  allowed extension
        if event.mimeData().urls():
            for u in event.mimeData().urls():
                u = u.toLocalFile()
                ext = u[u.rfind(".")+1:].lower()

                if ext in self.allowed_extensions:
                    self.list_model.insertRows({"path": u, "file_ext": ext})

            self.image_preview.emitter.added.emit()

    def load_project(self, project):
        """
        Invokes methods responsible for loading already existings project

        Argument/s:
            project - dictionary with info about projects (title, description,
            list with dictionaries that contains info about photos related to
            project)
        """
        self.list_model.load_project(project)
        self.file_widget.proj_wrapper.load_fields_data(
                                    project["title"], project["description"])
        self.image_preview.emitter.added.emit()

    def handle_add(self):
        """
        Handles emitter add signal. Opens file dialog and adds new images to
        project if some of theme have been chosen.
        """
        dialog = QtWidgets.QFileDialog.getOpenFileNames(self, "Wybierz zdjęcia",
                                filter="Pliki graficzne (*.jpg *.jpeg *.png)")

        if dialog[0]:
            for f in dialog[0]:
                data = {"path": f, "file_ext": f[f.rfind(".")+1:].lower()}
                self.list_model.insertRows(data)
            self.image_preview.emitter.added.emit()

    def handle_remove(self):
        """
        Handles emitter remove signal. Removes checked photos and updates image
        preview if actual image has been removed.
        """
        if self.list_model.handle_remove_data(self.image_preview.actual_image):
            self.image_preview.refresh_image(self.list_model.rowCount())

    @check_project_validity
    def handle_create(self):
        """
        Handles emitter create signal. Invokes methods responsible for creating
        project.
        """
        data = self.file_widget.proj_wrapper.extract_fields_data()
        temp, valid = QtWidgets.QInputDialog.getItem(self,"Wybierz szablon",
                            "Szablon", self.templates_names, 0, False)

        if temp and valid:
            data["path"] = self.database.directory
            data["template"] = temp.lower()
            self.set_progress_dialog("Przetwarzanie",
                                "Tworzenie projektu " + data["title"])
            self.progress_dial.setAutoClose(True)
            self.progress_dial.setValue(0)
            self.set_creator(data)

    def handle_progress(self):
        """
        Handles emitter progress signal. Increments value of progress dialog by
        one.
        """
        self.progress_dial.setValue(self.progress_dial.value() + 1)

    def handle_already_exists(self):
        """
        Handles emitter already exists signal. Displays message that album with
        that name already exists in project path.
        """
        self.display_message("Projekt o tej nazwie już istnieje.", stop=True)

    def handle_not_exist(self):
        """
        Handles emitter not exist signal. Displays message that one of the
        images related with project doesn't exist.
        """
        self.display_message("Jedno z wybranych zdjęć nie istnieje.", stop=True)

    def handle_finished(self):
        """
        Handles emitter finished signal. Displays message about successful
        project creation.
        """
        self.display_message("Projekt został stworzony pomyślnie.", "Ukończono")

    def handle_unknown_error(self):
        """
        Handles emitter unknown error signal. Displays message that unexpected
        error has occured.
        """
        self.display_message("Wystąpił błąd, spróbuj ponownie.", stop=True)


    def set_progress_dialog(self, title, info):
        """
        Creates progress dialog with passed info and displays it.

        Argument/s:
            title - title of progress dial
            info - text to display above the progress bar
        """
        self.progress_dial = QtWidgets.QProgressDialog(info,
                                "Anuluj", 0, len(self.list_model.data), self)
        self.progress_dial.setWindowTitle(title)
        self.progress_dial.setCancelButton(None)
        self.progress_dial.show()

    def set_creator(self, data):
        """
        Creates thread and project creator and starts it.

        Argument/s:
            data - dictionary with title, description and name of chosen
            template
        """
        handler = CreatorThread(parent=self)
        creator = ProjectCreator(self.list_model.data, data, self.emitter)
        handler.creator = creator
        handler.start()

    def start_new_project(self):
        """
        Starts new project by removing all rows from list model, refreshing
        image preview pixmap and clearing project info fields.
        """
        self.list_model.clear()
        self.database.current_project = None
        self.image_preview.refresh_image(0)
        self.file_widget.proj_wrapper.clear_fields_data()

    @check_project_validity
    @db_thread_wrapper
    def modify_project(self):
        """
        Checks current project status nad invokes method, dependently for saving
        or updating actual project.
        """
        data = self.file_widget.proj_wrapper.extract_fields_data()

        # If project is loaded and name of the project hasn't been changed,
        # update existing project.
        if self.list_model.loaded_project and not self.list_model.changed(data):
            self.update_project()

        # Else, save new one.
        else:
            self.save_project(data)

    def update_project(self):
        """
        Sets progress dialog with info that updating project is in process and
        invokes method responsible for updating project if content of project
        has been in some way changed (added image, deleted image etc.).
        """
        if self.list_model.updated:
            self.set_progress_dialog("Trwa aktualizowanie projektu",
                                                            "Proszę czekać")
            self.database.update_project(self.list_model, self.emitter)

    def save_project(self, data):
        """
        Sets progress dialog with info that saving project is in process and
        invokes method responsible for saving project.

        Argument/s:
            data - dictionary with title, description and name of chosen
            template
        """
        self.set_progress_dialog("Trwa zapisywanie projektu", "Proszę czekać")
        self.database.save_project(data, self.list_model, self.emitter)

    def handle_saved(self):
        """
        Handles emitter saved signal. Displays message that project has been
        saved properly and invokes method responsible for update list of
        projects.
        """
        self.display_message("Projekt został zapisany.", "Zapisano", True)
        self.parent().parent().parent().settings.handle_saved(
                        self.list_model.basic_info)

    def handle_updated(self):
        """
        Handles emitter saved signal. Displays message that project has been
        updated properly.
        """
        self.display_message("Projekt został zaktualizowny.", "Zaktualizowano",
                                                                        True)
        self.parent().parent().parent().settings.handle_updated(
            self.list_model.loaded_project["id"],
            self.list_model.loaded_project["last_active"]
        )


class ImageFileWidget(QtWidgets.QWidget):
    """
    Widget that works as a wrapper for project info wrapper, list view with
    images related with project and buttons, that allow to manipulate them.

    Attribute/s:
        proj_wrapper - instance of ProjectInfoWrapper
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.proj_wrapper = ProjectInfoWrapper(parent=self)

        list_view = QtWidgets.QListView(parent=self)
        list_view.setModel(self.parent().list_model)

        # Creating and connecting buttons that modifies image list and/or whole
        # project
        select_button = QtWidgets.QPushButton("Zaznacz/odznacz wszystko")
        select_button.clicked.connect(self.parent().list_model.handle_select)

        create_button = QtWidgets.QPushButton("Stwórz album")
        create_button.clicked.connect(self.parent().handle_create)

        add_button = QtWidgets.QPushButton("Dodaj zdjęcie")
        add_button.clicked.connect(self.parent().handle_add)

        remove_button = QtWidgets.QPushButton("Usuń zdjęcia")
        remove_button.clicked.connect(self.parent().handle_remove)

        vbox = QtWidgets.QVBoxLayout()
        vbox.addWidget(self.proj_wrapper)
        vbox.addWidget(list_view)
        vbox.addWidget(select_button)
        vbox.addWidget(add_button)
        vbox.addWidget(remove_button)
        vbox.addWidget(create_button)
        vbox.setAlignment(QtCore.Qt.AlignTop)

        self.setLayout(vbox)


class ProjectInfoWrapper(QtWidgets.QWidget):
    """
    Widget that works as a wrapper for line edits with title and description

    Attribute/s:
        title_line - instance of QLabel designed to contain title of project
        desc_line - instance of QLabel designed to contain description of
        project
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        title_label = QtWidgets.QLabel("Nazwa projektu: ")
        desc_label = QtWidgets.QLabel("Opis projektu: ")

        self.title_line = QtWidgets.QLineEdit()
        self.desc_line = QtWidgets.QLineEdit()

        grid = QtWidgets.QGridLayout()
        grid.addWidget(title_label, 0, 0)
        grid.addWidget(self.title_line, 0, 1)
        grid.addWidget(desc_label, 1, 0)
        grid.addWidget(self.desc_line, 1, 1)

        self.setLayout(grid)

    def extract_fields_data(self):
        """
        Return dictionary with formatted title and description.
        """
        return {
                "title": re.sub(r'\s\s+', '', self.title_line.text().strip()),
                "description": re.sub(r'\s\s+', '', self.desc_line.text().strip())
                }

    def validate_fields_data(self):
        """
        Validates title and description.
        """
        for value in self.extract_fields_data().values():
            value = value.lower()

            if len(value) == 0 or len(value) > 32 or not re.search(r'[a-z]', value):
                return False

        return True

    def load_fields_data(self, title, description):
        """
        Sets value of line edits.

        Argument/s:
            title - title of project
            description - description of project
        """
        self.title_line.setText(title)
        self.desc_line.setText(description)

    def clear_fields_data(self):
        """
        Cleans value of title and description line edits.
        """
        self.title_line.clear()
        self.desc_line.clear()


class ImagePreviewWidget(QtWidgets.QWidget):
    """
    Widget that works as a wrapper for image widget and image line wrapper

    Attribute/s:
        screen_geo - instance of ScreenGeometry that contains info about screen
        resolution
        actual_image - index of actual image
        list_model - instace of ImageListModel
        emitter - instance of ImageEmitter
        image_widget - instance of ActualImageWidget
        image_line_wrapper - instance of ActualImageLineWrapper
    """
    def __init__(self, geometry, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.screen_geo = geometry

        self.actual_image = 0
        self.list_model = self.parent().list_model

        # Configure emitter related with image preview (slider)
        self.emitter = ImageEmitter()
        self.emitter.changed.connect(self.changed_signal)
        self.emitter.added.connect(self.added_signal)

        vbox = QtWidgets.QVBoxLayout()

        self.image_widget = ActualImageWidget(parent=self)
        self.image_line_wrapper = ActualImageLineWrapper(parent=self)

        vbox.addWidget(self.image_widget)
        vbox.addWidget(self.image_line_wrapper)
        vbox.setAlignment(QtCore.Qt.AlignTop)
        vbox.setAlignment(QtCore.Qt.AlignCenter)
        self.setLayout(vbox)

    @property
    def actual_image_path(self):
        """
        Returns path to actual image
        """
        return self.parent().list_model.data[self.actual_image]["path"]

    def next_image(self):
        """
        Increments actual image number by 1. If number after incrementation is
        bigger than amount of images, actual image number is equal to 0.
        """
        if self.actual_image + 1 > self.parent().list_model.rowCount() - 1:
            self.actual_image = 0
        else:
            self.actual_image += 1

    def previous_image(self):
        """
        Decrements actual image number by 1. If number after decrementation is
        lesser than 0, actual image number is equal to amount of images.
        """
        if self.actual_image - 1 < 0:
            self.actual_image = self.parent().list_model.rowCount() - 1
        else:
            self.actual_image -= 1

    def set_image(self, value):
        """
        Sets image based on number entered to image line wrapper.

        Argument/s:
            value - value of image line wrapper
        """
        amount_of_images = self.parent().list_model.rowCount()

        if value < 1 or amount_of_images == 0:
            self.actual_image = 0
            self.image_line_wrapper.update_line()

        elif value > amount_of_images:
            self.actual_image = amount_of_images - 1
            self.image_line_wrapper.update_line(str(amount_of_images))

        else:
            self.actual_image = value - 1

        self.image_widget.set_pixmap_image()

    def refresh_image(self, list_len):
        """
        Refreshes image widget pixamp and image line wrapper value after
        deletion of actual image

        Argument/s:
            list_len - amount of images
        """
        if self.actual_image == 0 or self.actual_image in range(0, list_len):
            self.image_widget.set_pixmap_image()
            self.image_line_wrapper.update_line(str(self.actual_image + 1))

        else:
            self.actual_image -= 1
            return self.refresh_image(list_len)

    def changed_signal(self):
        """
        Updates image line wrapper value if one of the image widget buttons has
        been clicked.
        """
        self.image_line_wrapper.update_line(str(self.actual_image + 1))

    def added_signal(self):
        """
        Updates actual image, image line wrapper value and image widget pixmap
        if new image has been added.
        """
        self.actual_image = self.list_model.rowCount() - 1
        self.image_widget.set_pixmap_image()
        self.image_line_wrapper.update_line(str(self.actual_image + 1))


class ActualImageWidget(QtWidgets.QWidget):
    """
    Widget that contains actual image pixmap and buttons to change actual image.

    Attribute/s:
        image_label - instance of QLabel designed to contain pixmap with actual
        image
        pixmap - instance of QPixmap designed to contain actual image
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        #  Creating and configuring QLabel object that will work as wrapper of
        #  pixmap
        self.image_label = QtWidgets.QLabel()
        self.image_label.setMaximumWidth(self.parent().screen_geo.width()/2)
        self.image_label.setAlignment(QtCore.Qt.AlignCenter)

        self.pixmap = QtGui.QPixmap()
        self.set_pixmap_image()

        #  Creating and configuring buttons for slider (image preview)
        prev_button = QtWidgets.QPushButton("<<")
        prev_button.setFixedWidth(60)
        prev_button.clicked.connect(self.get_previous_image)

        next_button = QtWidgets.QPushButton(">>")
        next_button.setFixedWidth(60)
        next_button.clicked.connect(self.get_next_image)

        hbox = QtWidgets.QHBoxLayout()
        hbox.addWidget(prev_button)
        hbox.addWidget(self.image_label)
        hbox.addWidget(next_button)

        self.setLayout(hbox)

    def set_pixmap_image(self):
        """
        Sets pixmap image to actual image. If actual image has been deleted,
        display image with error. If any image hasn't been loaded yet, display
        image with info how to do that.
        """
        try:
            self.pixmap.load(self.parent().actual_image_path)

            if self.pixmap.isNull():
                self.pixmap.load(os.path.join(BASE_DIR, "assets/404.png"))

        except IndexError:
            self.pixmap.load(os.path.join(BASE_DIR, "assets/default.png"))

        geo = self.parent().screen_geo
        self.pixmap = self.pixmap.scaled(geo.width()/2.25, geo.height()/2,
                                                    QtCore.Qt.KeepAspectRatio)
        self.image_label.setPixmap(self.pixmap)

    @update_image
    def get_next_image(self):
        """
        Gets next image
        """
        self.parent().next_image()

    @update_image
    def get_previous_image(self):
        """
        Gets previous image
        """
        self.parent().previous_image()


class ActualImageLineWrapper(QtWidgets.QWidget):
    """
    Widget that works as a wrapper for line edit with value equal to actual
    number and confirm button.

    Attribute/s:
        actual_image_line_edit - instance of QLineEdit designed to contain
        number of actual image
        button - instance of QPushButton for invoking function responsible for
        changing to that typed into the actual image line edit
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.actual_image_line_edit = QtWidgets.QLineEdit()
        self.actual_image_line_edit.setFixedWidth(34)
        self.update_line()

        self.button = QtWidgets.QPushButton("Przejdź")
        self.button.setFixedWidth(80)
        self.button.clicked.connect(self.update_image)

        grid = QtWidgets.QGridLayout()
        grid.addWidget(self.actual_image_line_edit, 0, 0)
        grid.addWidget(self.button, 0, 1)
        grid.setAlignment(QtCore.Qt.AlignTop)
        grid.setAlignment(QtCore.Qt.AlignCenter)
        self.setLayout(grid)

    def update_line(self, index='1'):
        """
        Updates line edit value based on image preview button click event or
        user out of range typed value.

        Argument/s:
            index - actual image value
        """
        self.actual_image_line_edit.setText(index)

    def update_image(self):
        """
        Updates actual image based on user typed value. If user doesn't type
        integer value, message box with error is displayed
        """
        try:
            typed_value = int(self.actual_image_line_edit.text())

        except ValueError:
            self.parent().parent().display_message(
                                        "Wprowadzona liczba nie jest poprawna")

            return None

        self.parent().set_image(typed_value)
