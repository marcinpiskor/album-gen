from PyQt5 import QtWidgets, QtGui, QtCore
from .list_model import ProjectsListModel
from .decorators import db_thread_wrapper, MessageMixin
from .emitters import SettingsEmitter


class SettingsWidget(MessageMixin, QtWidgets.QWidget):
    """
    Widget that works as a wrapper for configure and project manager system.

    Attribute/s:
        database - DatabaseHandler object
        config - ConfigWrapperWidget object
        proj_manager - ProjectsManagerWidget object
    """
    def __init__(self, database, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.database = database

        self.emitter = SettingsEmitter()
        self.emitter.error.connect(self.handle_error)

        self.config = ConfigWrapperWidget(parent=self)
        self.proj_manager = ProjectsManagerWidget(parent=self)

        hbox = QtWidgets.QHBoxLayout(self)
        hbox.addWidget(self.config)
        hbox.addWidget(self.proj_manager)
        hbox.setAlignment(QtCore.Qt.AlignTop)

    def handle_saved(self, new_proj):
        """
        Invokes method responsible for adding just created project to projects
        list model.

        Argument/s:
            new_proj - dictionary with info about project(title, description,
            last time when project was active)
        """
        self.proj_manager.list_model.insertRows(new_proj)

    def handle_updated(self, proj_id, proj_active):
        """
        Invokes method responsible for updating last active value of just
        updated project.

        Argument/s:
            proj_id - id of updated project
            proj_active - time when project has been updated
        """
        self.proj_manager.list_model.update_project_active(proj_id, proj_active)

    def handle_error(self):
        """
        Invokes method responsible for displaying message box with passed error
        info.
        """
        self.display_message("Wystąpił błąd podczas połączanie z bazą danych")

    @db_thread_wrapper
    def load_project(self):
        """
        Invokes methods responsible for loading project.
        """
        to_load = self.proj_manager.list_model.handle_load()

        if to_load:
            proj = self.database.load_project(to_load)
            self.parent().parent().parent().image_manager.load_project(proj)
            self.parent().parent().setCurrentIndex(0)

    @db_thread_wrapper
    def delete_project(self):
        """
        Invokes methods responsible for deleting project.
        """
        to_del = self.proj_manager.list_model.handle_delete(
            self.database.current_project
        )

        if len(to_del) > 0:
            self.database.delete_projects(to_del)

    @db_thread_wrapper
    def change_directory(self):
        """
        Opens directory file dialog, if user has chosen different path from
        actual, invokes method responsible for updating project directory.
        """
        new_dir = QtWidgets.QFileDialog.getExistingDirectory(
            self,
            "Wybierz folder",
            self.database.directory[:self.database.directory.rfind('/')],
            QtWidgets.QFileDialog.ShowDirsOnly
        )

        if new_dir:
            self.database.update_directory(new_dir)
            self.config.set_directory_info()


class ConfigWrapperWidget(QtWidgets.QWidget):
    """
    Widget that works as a wrapper for configuration buttons.

    Argument/s:
        actual_dir - QLabel object that stores info about actual project path
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.actual_dir = QtWidgets.QLabel()
        self.set_directory_info()

        dir_button = QtWidgets.QPushButton(self, text="Wybierz lokalizację")
        dir_button.clicked.connect(self.parent().change_directory)

        vbox = QtWidgets.QVBoxLayout(self)
        vbox.addWidget(self.actual_dir)
        vbox.addWidget(dir_button)
        vbox.setAlignment(QtCore.Qt.AlignTop)

    def set_directory_info(self):
        """
        Sets label text value to current project path (directory).
        """
        self.actual_dir.setText("<b>Aktualna lokalizacja:</b> " +
                                            self.parent().database.directory)


class ProjectsManagerWidget(QtWidgets.QWidget):
    """
    Widget that works as a wrapper for project list view and buttons for
    managing projects.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.list_model = ProjectsListModel(parent=self)

        list_view = QtWidgets.QListView(parent=self)
        list_view.setModel(self.list_model)

        #  Creating and configurating buttons responsible for projects managing
        load_proj_button = QtWidgets.QPushButton(parent=self,
                                                        text="Załaduj projekt")
        load_proj_button.clicked.connect(self.parent().load_project)

        delete_proj_button = QtWidgets.QPushButton(parent=self,
                                                        text="Usuń projekt")
        delete_proj_button.clicked.connect(self.parent().delete_project)

        vbox = QtWidgets.QVBoxLayout(self)
        vbox.addWidget(list_view)
        vbox.addWidget(load_proj_button)
        vbox.addWidget(delete_proj_button)
        vbox.setAlignment(QtCore.Qt.AlignTop)
