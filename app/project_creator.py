import os
import re
import json
from PIL import Image
from shutil import copy, copytree, rmtree
from PyQt5 import QtCore

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

class CreatorThread(QtCore.QThread):
    """
    Thread that works as a wrapper for creator project class

    Attribute/s:
        creator - instance of ProjectCreator, prepared to build project.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.creator = None

    def run(self):
        try:
            self.creator.build_project()

        except FileExistsError:
            self.parent().emitter.already_exists.emit()

        except FileNotFoundError:
            self.creator.remove_project()
            self.parent().emitter.not_exist.emit()

        except:
            self.creator.remove_project()
            self.parent().emitter.unknown_error.emit()


class ProjectCreator:
    """
    Class responsible for creating project

    Attribute/s:
        formats - dictionary with info about image convertion mode related with
        particular extension
        image_list - list with dictionaries that contain info about images
        related with project
        converted_list - list for dictionaries that contain info about already
        prepared images
        template_path - path to chosen template
        project_path - path to directory where project will be placed
        project_data - dictionary that contains info about project data
        (title, description, template name)
        emitter - manager emitter object of image manager widget
    """
    formats = {"jpeg": "JPEG", "jpg": "JPEG", "png": "PNG"}

    def __init__(self, image_list, project_data, emitter):
        self.image_list = image_list
        self.converted_list = []
        self.template_path = "{0}/templates/{1}/".format(BASE_DIR,
                                                project_data.pop("template"))
        self.project_path = "{0}/{1}/".format(project_data.pop('path'),
                                    re.sub(r'/', '-', project_data["title"]))
        self.project_data = project_data
        self.emitter = emitter

    def build_project(self):
        """
        Invokes all necessary methods to create a project
        """
        self.copy_template_structure()

        for index in range(len(self.image_list)):
            self.prepare_image(self.image_list[index], index)

        self.build_template()
        self.emitter.finished.emit()

    def copy_template_structure(self):
        """
        Copies all source files of particular template to project path
        """
        copytree(self.template_path + "assets", self.project_path + "assets")

    def prepare_image(self, image_dict, index):
        """
        Copies particular image to project path, makes its thumbnail version and
        adds dictionary with info about image and image thumbnail paths to list.

        Argument/s:
            image_dict - dictionary with info about particular
            index - index of particular image
        """
        normal_path = "assets/img/{0}.{1}".format(index, image_dict["ext"])
        thumb_path = "assets/img/{0}_thumb.{1}".format(index, image_dict["ext"])

        copy(image_dict["path"], self.project_path + normal_path)

        with Image.open(self.project_path + normal_path) as im:
            im.thumbnail(self.get_thumb_size(im), Image.ANTIALIAS)
            im.save(self.project_path + thumb_path,
                                    self.formats[image_dict["ext"]])

        self.converted_list.append({"normal": normal_path, "thumb": thumb_path})
        self.emitter.progress.emit()

    @staticmethod
    def get_thumb_size(image):
        """
        Returns size of image thumbnail dependently on that if image is portrait
        or landscape.

        Argument/s:
            image - PIL Image object
        """
        width, height = image.size

        if width > height:
            return (500, 280)
        else:
            return (280, 500)

    def build_template(self):
        """
        Makes HTML template based on chosen template and provided title,
        description and images.
        """
        self.project_data["images"] = json.dumps(self.converted_list)

        with open(self.template_path + "index.html", "r", encoding="utf-8") as t:
            with open(self.project_path + "index.html", "w", encoding="utf-8") as f:
                f.write(t.read().format(**self.project_data))

    def remove_project(self):
        """
        Removes project folder
        """
        rmtree(self.project_path)
