import os
from datetime import datetime
import sqlite3 as lite
from PyQt5 import QtCore
from .project_creator import BASE_DIR

def con_wrapper(func):
    """
    Decorator that provides the methods connection to database.

    Argument/s:
        func - function to wrap
    """
    def wrapper(self, *args, **kwargs):
        connect = lite.connect(self.database_name)

        with connect:
            cursor = connect.cursor()
            result = func(self, cursor, *args, **kwargs)

            return result

    return wrapper


class DatabaseHandler:
    """
    Class responsible for making operations on config database that contains
    settings of main app

    Attribute/s:
        database_name - name of database to connect with
        directory - path to directory where all projects created in future will
        be placed
        default_directory - path to default directory (projects folder in main
        folder of program)
        current_project - id of current loaded project
    """
    def __init__(self, database_name="config.db"):
        self.database_name = database_name
        self.directory = None
        self.default_directory = BASE_DIR + "/projects"
        self.current_project = None

        self.get_directory()
        self.check_directory()
        self.init_project_structure()

    @con_wrapper
    def get_directory(self, cursor):
        """
        Gets current directory where projects are stored.
        If projectpath table hasn't been already created, creates it and sets
        directory to default directory.

        Argument/s:
            cursor - Cursor object
        """
        try:
            cursor.execute("SELECT * FROM projectpath LIMIT 1;")
            self.directory = cursor.fetchone()[0]

        except lite.OperationalError:
            cursor.execute("CREATE TABLE projectpath (path TEXT);")
            cursor.execute("INSERT INTO projectpath VALUES('{0}');".format(
                                                    self.default_directory))

            self.directory = self.default_directory

    @con_wrapper
    def update_directory(self, cursor, directory):
        """
        Updates directory where projects are stored.

        Argument/s:
            cursor - Cursor object
            directory - path to new directory
        """
        self.directory = directory
        self.check_directory()

        cursor.execute("UPDATE projectpath SET path='{0}'".format(directory))

    @con_wrapper
    def init_project_structure(self, cursor):
        """
        Creates project and photo tables if these tables haven't been already
        created.

        Argument/s:
            cursor - Cursor object
        """
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS project (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                title VARCHAR(48),
                description VARCHAR(48),
                last_active DATETIME
            )
            """
        )

        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS photo (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                path TEXT,
                project_id INTEGER,
                FOREIGN KEY (project_id) REFERENCES project(id)
            )
            """
        )

    @con_wrapper
    def save_project(self, cursor, project, list_model, emitter):
        """
        Saves new project and photos related to it to database.

        Argument/s:
            cursor - Cursor object
            project - dictionary with info about title and description of
            project
            list_model - ImageListModel object
            emitter - ManagerEmitter object
        """
        project["last_active"] = datetime.now().strftime('%d.%m.%Y, %H:%M:%S')

        cursor.execute(
            """
            INSERT INTO project ('title', 'description', 'last_active')
            VALUES('{0}', '{1}', '{2}');
            """
            .format(
                project["title"],
                project["description"],
                project["last_active"]
            )
        )

        cursor.execute("SELECT id FROM project ORDER BY id DESC LIMIT 1;")

        project["id"] = cursor.fetchone()[0]
        self.current_project = project["id"]

        self.save_photos(cursor, list_model.data, project["id"])

        cursor.execute("SELECT id FROM photo WHERE project_id={0};".format(
            project["id"]
        ))

        # Invoking list model method responsible for setting id numbers of
        # images. This is necessary if user would want to update project without
        # necessity of loading whole project again after save.
        list_model.update_data_id(cursor.fetchall(), 'save')
        list_model.loaded_project = project
        list_model.clear_project()

        emitter.saved.emit()

    @con_wrapper
    def update_project(self, cursor, list_model, emitter):
        """
        Updates existing project by deleting removed photos from database and
        saving new photos to database related with current loaded project.

        Argument/s:
            cursor - Cursor object
            list_model - ImageListModel object
            emitter - ManagerEmitter object
        """
        list_model.loaded_project["last_active"] = datetime.now().strftime(
                                                        '%d.%m.%Y, %H:%M:%S')

        self.set_project_last_active(cursor, list_model.loaded_project["id"],
                                    list_model.loaded_project["last_active"])

        if list_model.loaded_project["added"]:
            self.save_photos(cursor, list_model.loaded_project["added"],
                                            list_model.loaded_project["id"])

        if list_model.loaded_project["removed"]:
            self.delete_photos(cursor, list_model.loaded_project["removed"])

        # Getting id of all photos that has been added to project
        cursor.execute(
            "SELECT id FROM photo WHERE project_id={0} ORDER BY id DESC LIMIT {1};"
            .format(
                list_model.loaded_project["id"],
                len(list_model.loaded_project["added"])
            )
        )

        # Invoking list model method responsible for setting id numbers of
        # images. This is necessary if user would want to update project without
        # necessity of loading whole project again after update.
        list_model.update_data_id(cursor.fetchall()[::-1], "update")
        list_model.clear_project()

        emitter.updated.emit()

    @staticmethod
    def set_project_last_active(cursor, proj_id, last_active):
        """
        Updates last active column value of project.

        Argument/s:
            cursor - Cursor object
            proj_id - id of project
            last_active - new value of last active columne
        """
        cursor.execute(
            "UPDATE project SET last_active='{0}' WHERE id={1}".format(
                last_active, proj_id
            )
        )

    @staticmethod
    def save_photos(cursor, photos, proj_id):
        """
        Saves new photos to database and relates them to project with passed id.

        Argument/s:
            photos - list with dictionary that contains info about images
            proj_id - id of project that will be related with photos
        """
        to_save = ["('{0}', {1})".format(p["path"], proj_id) for p in photos]

        cursor.execute(
            "INSERT INTO photo ('path', 'project_id') VALUES {};"
            .format(", ".join(to_save))
        )

    @staticmethod
    def delete_photos(cursor, photos):
        """
        Deletes photos that have been deleted from image list and have been
        already saved and related to project.

        Argument/s:
            cursor - Cursor object
            photos - list with id of deleted photos
        """
        cursor.execute(
            "DELETE FROM photo WHERE id IN ({0});".format(", ".join(photos))
        )

    @con_wrapper
    def load_projects(self, cursor):
        """
        Loads and returns projects.

        Argument/s:
            cursor - Cursor object
            limit - amount of projects to load
        """
        cursor.execute(
        "SELECT id, title, last_active FROM project ORDER BY last_active DESC;"
        )

        return self.zip_fetch(['id', 'title', 'last_active'], cursor.fetchall())

    @con_wrapper
    def load_project(self, cursor, proj_id):
        """
        Loads project with passed id.

        Argument/s:
            cursor - Cursor object
            proj_id - id of project to load
        """
        cursor.execute("SELECT * FROM project WHERE id={0};".format(proj_id))

        keys = ['id', 'title', 'description', 'last_active']
        loaded_proj = self.zip_fetch(keys, [cursor.fetchone()])[0]

        self.current_project = loaded_proj["id"]

        cursor.execute(
            "SELECT id, path FROM photo WHERE project_id={}".format(proj_id)
        )

        loaded_proj["photos"] = self.zip_fetch(['file_id', 'path'],
                                                            cursor.fetchall())

        return loaded_proj

    @con_wrapper
    def delete_projects(self, cursor, projects_id):
        """
        Deletes all projects with passed id and all images related to that
        projects.
        """
        cursor.execute(
            "DELETE FROM project WHERE id IN ({0});".format(projects_id)
        )
        cursor.execute(
            "DELETE FROM photo WHERE project_id IN ({0});".format(projects_id)
        )

    def check_directory(self):
        """
        Checks if directory exists, if not sets directory to default directory.
        """
        if not os.path.isdir(self.directory):
            self.directory = self.default_directory

    @staticmethod
    def zip_fetch(keys, values):
        """
        Zips tuples with info about projects with proper keys and converts it to
        dictionary.

        Argument/s:
            projects - list with tuples with info about projects
        """
        return [dict(zip(keys, v), checked=False) for v in values]


class DatabaseThread(QtCore.QThread):
    """
    Thread that works as a wrapper for load, save, update and delete project to
    database.
    """
    def __init__(self, action, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.action = action

    def run(self, *args, **kwargs):
        try:
            self.action(*args, **kwargs)
        except:
            self.parent().emitter.error.emit()
