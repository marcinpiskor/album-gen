from PyQt5 import QtCore


class ManagerEmitter(QtCore.QObject):
    """
    Works as a signals emitter for image manager widget.
    """
    finished = QtCore.pyqtSignal()
    progress = QtCore.pyqtSignal()
    already_exists = QtCore.pyqtSignal()
    not_exist = QtCore.pyqtSignal()
    unknown_error = QtCore.pyqtSignal()
    saved = QtCore.pyqtSignal()
    updated = QtCore.pyqtSignal()


class ImageEmitter(QtCore.QObject):
    """
    Works as a signals emitter for image preview widget.
    """
    changed = QtCore.pyqtSignal()
    added = QtCore.pyqtSignal()


class SettingsEmitter(QtCore.QObject):
    """
    Works as a signals emitter for settings widget.
    """
    error = QtCore.pyqtSignal()
