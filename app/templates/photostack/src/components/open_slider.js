import globals from "./private_globals";

function open_slider() {
  /**
  * Opens slider and sets image of it.
  */
  let slider = document.querySelector(".slider"), img = document.querySelector("img");

  img.onload = () => {
    slider.childNodes[1].className = 0;
    img.style.opacity = "1";
    if (slider.className.indexOf("visible") === -1) slider.className += " visible";
  };

  img.src = images[globals.min].normal;
}

module.exports = open_slider;
