function fade_polaroid(polaroid) {
  /**
  * Moves polaroid down and fades it out.
  * @param { Element } polaroid - div elementh with polaroid class
  */
  polaroid.className = "polaroid";
  polaroid.style.top = parseInt(polaroid.style.top) + 20 + "px";
}

module.exports = fade_polaroid;
