import globals from "./private_globals";

function init_images() {
  /**
  * Sets background image of created plaroids.
  */
  images.slice(globals.min, globals.max+1).reverse().forEach((im, i) => {
    globals.polaroids[i].style.backgroundImage = `url(${im.thumb})`;
  });
}

module.exports = init_images;
