import globals from "./private_globals";
import create_polaroid from "./create_polaroid";

function init_polaroids() {
  /**
  * Invokes function responsible for creating polaroids and appends them to body
  */
  for (let i=0; i <= globals.max; i++) {
    let pol = create_polaroid(i);
    document.querySelector("main").appendChild(pol);
  }

  globals.polaroids = Array.from(document.querySelectorAll(".polaroid"));
}

module.exports = init_polaroids;
