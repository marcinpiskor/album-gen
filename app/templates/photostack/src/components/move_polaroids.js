import globals from "./private_globals";

function move_polaroids(up) {
  /**
  * Updates order of polaroid by incrementing or decrementing polaroid's id
  * attribute.
  * @param { Boolean } up - decides if ids of polaroids going up or down
  */
  globals.polaroids.forEach(pol => {
    let actual = parseInt(pol.getAttribute("data-id"));
    actual += up ? -1 : 1;
    pol.style.top = `${actual*20}px`;
    pol.setAttribute("data-id", actual);
  });
}

module.exports = move_polaroids;
