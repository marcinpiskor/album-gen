import globals from "./private_globals";
import create_polaroid from "./create_polaroid";
import move_polaroids from "./move_polaroids";
import fade_polaroid from "./fade_polaroid";
import update_slider from "./update_slider";

let main = document.querySelector("main");

function next_polaroid() {
  /**
  * Removes first polaroid, append new one to the end and changes order of all
  * polaroids.
  */
  if (globals.min + 1 !== images.length && main.className === "") {
    main.className += "in-process";

    globals.min++;
    globals.max++;

    fade_polaroid(globals.polaroids[globals.polaroids.length - 1]);
    update_slider();

    setTimeout(() => {
      main.removeChild(globals.polaroids.pop());

      if (globals.max <= images.length - 1) {
        move_polaroids(false);

        let new_polaroid = create_polaroid(0);
        new_polaroid.style.backgroundImage = `url(${images[globals.max].thumb})`;

        main.insertBefore(new_polaroid, globals.polaroids[0]);
        globals.polaroids.unshift(new_polaroid);
      }

      main.className = "";
    }, 100)
  }
}

function prev_polaroid() {
  /**
  * Removes last polaroid, append new one to the start and changes order of all
  * polaroids.
  */
  if (globals.min - 1 >= 0 && main.className === "") {
    main.className += "in-process";

    globals.min--;
    globals.max--;

    if (globals.max < images.length - 1) fade_polaroid(globals.polaroids[0]);
    update_slider();

    setTimeout(() => {
      let pol_id = globals.amount_of_polaroids - (globals.max - images.length + 1);

      if (globals.max < images.length - 1) {
        main.removeChild(globals.polaroids.shift());
        move_polaroids(true);
        pol_id = 4;
      }

      let new_polaroid = create_polaroid(pol_id);
      new_polaroid.style.backgroundImage = `url(${images[globals.min].thumb})`;

      main.appendChild(new_polaroid);
      globals.polaroids.push(new_polaroid);

      main.className = "";
    }, 100);
  }
}

module.exports = {prev_polaroid: prev_polaroid, next_polaroid: next_polaroid};
