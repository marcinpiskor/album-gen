let globals = {
  min: 0,
  max: images.length < 5 ? images.length - 1 : 4,
  polaroids: null
}

globals.amount_of_polaroids = globals.max;

module.exports = globals;
