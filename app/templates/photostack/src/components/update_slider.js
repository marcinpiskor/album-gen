import globals from "./private_globals";

function update_slider() {
  /**
  * Updates slider image after.
  */
  let slider = document.querySelector(".slider");

  if (slider.className.indexOf("visible") != -1) {
    slider.childNodes[1].className = "loading";

    let img = document.querySelector("img");
    img.style.opacity = "0";

    setTimeout(() => img.src = images[globals.min].normal, 500);
  }
}

module.exports = update_slider;
