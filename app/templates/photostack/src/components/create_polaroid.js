import open_slider from "./open_slider";

function create_polaroid(index) {
  /**
  * Creates new div element with polaroid class
  * @param { Number } index - index of polaroid
  * @return { Element } - div element with polaroid class
  */
  let pol = document.createElement("div");

  pol.setAttribute("data-id", index);
  pol.style.top = index * 20 + "px";
  pol.className = "polaroid";
  pol.addEventListener("click", open_slider);

  setTimeout(() => pol.className += " visible", 100);

  return pol;
}

module.exports = create_polaroid;
