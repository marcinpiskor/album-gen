import "babel-polyfill";
import init_polaroids from "./components/init_polaroids";
import init_images from "./components/init_images";
import {prev_polaroid, next_polaroid} from "./components/change_polaroid";

window.onload = () => {
  init_polaroids();
  init_images();

  // BUTTON CLOSE SLIDER SUPPORT
  document.querySelector(".close-button").addEventListener("click", () => {
    document.querySelector(".slider").className = "slider";
  });

  // SLIDER BUTTONS SUPPORT
  document.querySelector("#next-slide").addEventListener("click", next_polaroid);
  document.querySelector("#prev-slide").addEventListener("click", prev_polaroid);

  // NORMAL BUTTONS SUPPORT
  document.querySelector("#next").addEventListener("click", next_polaroid);
  document.querySelector("#prev").addEventListener("click", prev_polaroid);

  // TOUCH EVENTS SLIDER SUPPORT
  let slider = document.querySelector(".slider"), first_touch = {};

  slider.addEventListener("touchstart", (event) => {
     first_touch = event;
  });

  slider.addEventListener("touchend", (event) => {
    let move_y = Math.abs(event.changedTouches[0].screenY - first_touch.y);

    if (event.timeStamp - first_touch.timestamp <= 1000 && move_y < 30) {
      let move_x = event.changedTouches[0].screenX - first_touch.x;

      if (move_x > 0) {
        prev_polaroid()
      }
      else if (move_x < 0){
        next_polaroid()
      }
    }
  });

  // KEYBOARD ARROWS SUPPORT
  document.querySelector("body").addEventListener("keydown", (event) => {
    switch(event.key) {
      case "ArrowLeft":
        prev_polaroid();
        break;
      case "ArrowRight":
        next_polaroid();
        break;
    };
  });
}
