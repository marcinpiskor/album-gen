var gulp = require('gulp');
var fs = require('fs');
var browserify = require('browserify');
var minify = require('gulp-minify');
var sass = require('gulp-sass');

gulp.task('default', function() {
  browserify("./src/index.js")
    .transform("babelify", {presets: ["es2015"]})
    .bundle()
    .pipe(fs.createWriteStream("./assets/js/app.js"));
});

gulp.task('jsmin', function() {
  gulp.src("./assets/js/app.js")
    .pipe(minify({
        ext: {
          src: '.js',
          min: '-min.js'
        }
    }))
    .pipe(gulp.dest("./assets/js/"));
})

gulp.task('sass', function() {
  return gulp.src('./sass/style.sass')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest("./assets/css/"))
});
