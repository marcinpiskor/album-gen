import "babel-polyfill";
import {previous_slide, next_slide} from './components/change_slide';
import get_images from "./components/get_images";
import button_load from "./components/button_load";

window.onload = () => {
  get_images();

  let slider = document.querySelector(".slider");

  // HANDLING CLICKING CLOSE BUTTON RESPONSIBLE FOR LOADING NEXT IMAGES
  document.querySelector("button").addEventListener("click", button_load);

  // HANDLING CLICKING BUTTON RESPONSIBLE FOR CLOSING SLIDER
  document.querySelector(".close").addEventListener("click",
    () => document.querySelector(".slider").className = "slider");

  // ARROW BUTTONS SUPPORT FOR SLIDE CHANGE
  document.getElementById("left-arrow").addEventListener("click", previous_slide);
  document.getElementById("right-arrow").addEventListener("click", next_slide);

  // TOUCH SUPPORT FOR SLIDE CHANGE
  let touch = {};

  slider.addEventListener("touchstart", event => touch = event);
  slider.addEventListener("touchend", event => {
    let move_y = Math.abs(event.changedTouches[0].screenY - touch.touches[0].screenY);

    if (event.timeStamp - touch.timeStamp <= 1000 && move_y < 30) {
      let move_x = event.changedTouches[0].screenX - touch.touches[0].screenX;

      if (move_x > 0) {
        previous_slide()
      }
      else if (move_x < 0){
        next_slide()
      }
    }
  });

  // KEYBOARD ARROWS SUPPORT FOR SLIDE CHANGE
  document.querySelector("body").addEventListener("keydown", event => {
    if (document.querySelector(".slider").className.indexOf("visible") !== -1) {
      switch(event.key) {
        case "ArrowLeft":
          previous_slide();
          break;
        case "ArrowRight":
          next_slide();
          break;
      };
    }
  });
}
