import get_images from "./get_images";

function button_load(event) {
  /**
  * Invokes function responsible for loading next image and adds to button
  * class with animation
  * @param { Object } event - JS event click object.
  */
  event.target.className = "loading-process";

  setTimeout(() => {
    get_images();
    event.target.className = "";
  }, 800);
}

module.exports = button_load;
