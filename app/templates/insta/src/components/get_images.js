import image_generator from "./image_generator";
import add_image from "./add_image";
import load_image from "./load_image";

let iter_count = 0, done = false;
const generator = image_generator(images);

function get_images() {
  /**
  * Handles image generator. Adds images to document with use of add function.
  */
  if (!done) {
    let promise_arr = [];

    for (let i = 1; i % 7 != 0; i++) {
      let image_obj = generator.next();

      if (image_obj.done) {
        done = true;
        break;
      }

      promise_arr.push(load_image(image_obj.value))
    }

    // Image boxes need to have attribute with index of image for slider.
    // However, promise array can possess at most 6 image objects. To provide
    // proper index values, we need to calculate how many images have been
    // already displayed, based on iteration count value.

    Promise.all(promise_arr)
      .then(images => images.forEach((image, i) => add_image(image, i + (iter_count * 6))))
      .then(() => iter_count++)
  }
}

module.exports = get_images;
