import open_slider from './open_slider';

function add_image(obj, index) {
  /**
  * Adds image block to box section and sets its background image and data-id
  * that corresponds to index of image object in array.
  * @param { Object } obj - image object that contain path to normal image and
  * thumbnail of it.
  * @param { Number } index - index of image object in array.
  */
  let image_block = document.createElement('div');

  image_block.className = "image";
  image_block.addEventListener("click", open_slider)
  image_block.style.backgroundImage = `url("${obj.thumb}")`;
  image_block.setAttribute('data-id', index);

  document.querySelector(".box").appendChild(image_block);
}

module.exports = add_image;
