import globals from "./private_globals";
import update_slide from "./update_slide";

function previous_slide() {
  /**
  * Decrements slider image index if actual image is not the first one.
  */
  if (globals.slider_image - 1 >= 0) globals.slider_image -= 1; update_slide();
}

function next_slide() {
  /**
  * Increments slider image index if actual image is not the last one.
  * If slide image is last one currently loaded on page, button that is attached
  * to loading images function is clicked.
  */
  if (globals.slider_image + 1 === images.length) {
    globals.slider_image = 0;
  }
  else {
    if ((globals.slider_image + 1) % 6 === 0) {
       document.querySelector("button").click();
    }

    globals.slider_image += 1;
  }

  update_slide()
}

module.exports = {previous_slide: previous_slide, next_slide: next_slide};
