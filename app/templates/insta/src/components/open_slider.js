import globals from "./private_globals";
import update_slide from "./update_slide";

function open_slider(event) {
  /**
  * Opens slider and sets image of it based on clicked image block.
  * @param { Object } event - JS event object
  */
  document.querySelector(".slider").className += " visible";

  globals.slider_image = parseInt(event.target.getAttribute("data-id"));
  update_slide(true);
}

module.exports = open_slider;
