import globals from "./private_globals";

function update_slide(open=false) {
  /**
  Sets slider image based on actual slider image index.
  @param { Boolean } open - holds info if slider has to be opened
  */
  let image = document.querySelector("img");
  let spinner = document.querySelector(".spinner");

  image.onload = () => spinner.className = "spinner";

  spinner.className += " loading-image";
  image.src = images[globals.slider_image].normal;
}

module.exports = update_slide;
