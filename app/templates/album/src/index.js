import "babel-polyfill";
import globals from "./components/private_globals";
import {previous_slide, next_slide} from "./components/change_slide";

window.onload = () => {
  // HANDLING CLICKING BUTTON RESPONSIBLE FOR CLOSING SLIDER
  document.querySelector("button").addEventListener("click",
    () =>  document.querySelector(".slider").className = "slider")

  // TOUCH SUPPORT FOR SLIDE CHANGE
  let image_roll = document.querySelector(".image-roll"), first_touch = {};

  image_roll.addEventListener("touchstart", event => {
    first_touch = event;
  }, false);

  image_roll.addEventListener("touchend", event => {
    let move_y = Math.abs(event.changedTouches[0].screenY - first_touch.touches[0].screenY);

    if (move_y < 30) {
      let move_x = event.changedTouches[0].screenX - first_touch.touches[0].screenX;

      if (move_x > 0) {
        previous_slide(true)
      }
      else if (move_x < 0){
        next_slide(true)
      }
    }
    else {
      image_roll.style.transform = `translateX(-${100*globals.actual_slide}%)`;
    }
  }, false);

  // ARROW BUTTONS SUPPORT FOR SLIDE CHANGE
  document.getElementById("left-arrow").addEventListener("click", () => previous_slide());
  document.getElementById("right-arrow").addEventListener("click", () => next_slide());

  // KEYBOARD ARROWS SUPPORT FOR SLIDE CHANGE
  document.querySelector("body").addEventListener("keydown", (event) => {
    if (document.querySelector(".slider").className.indexOf("visible") == -1) {
      switch(event.key) {
        case "ArrowLeft":
          previous_slide();
          break;
        case "ArrowRight":
          next_slide();
          break;
      };
    }
  });
}
