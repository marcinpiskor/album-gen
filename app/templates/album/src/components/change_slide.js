import globals from "./private_globals";
import get_slides from "./get_slides";

function previous_slide(touch=false) {
  /**
  * Decrements slider image index if actual image is not the first one.
  * @param { Boolean } touch - if function is invoked by touch events
  */
  let image_roll = document.querySelector(".image-roll");

  if (globals.actual_slide - 1 < 0) {
    if (!touch) image_roll.style.transform = "translateX(10%)";
    setTimeout(() => image_roll.style.transform = "translateX(0%)", 400);
  }
  else {
    globals.actual_slide--;
    image_roll.style.transform = `translateX(-${100*globals.actual_slide}%)`;
  }
}

function next_slide(touch=false) {
  /**
  * Increments slider image index if actual image is not the last one.
  * @param { Boolean } touch - if function is invoked by touch events
  */
  let image_roll = document.querySelector(".image-roll");
  
  if (globals.actual_slide === Math.ceil(images.length/4) + 1) {
    if (!touch) image_roll.style.transform = `translateX(-${(100*globals.actual_slide)+10}%)`;
    setTimeout(() => image_roll.style.transform = `translateX(-${(100*globals.actual_slide)}%)`, 400);
  }
  else {
    globals.actual_slide++;
    if (document.querySelector(`div[data-id='${globals.actual_slide}']`) === null) get_slides(globals.actual_slide);
    else image_roll.style.transform = `translateX(-${100*globals.actual_slide}%)`;
  }
}

module.exports = {previous_slide: previous_slide, next_slide: next_slide};
