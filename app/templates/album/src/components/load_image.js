function load_image(obj) {
  /**
  * Checks if image is valid.
  * @param { Object } obj - image object that contain path to normal image and
  * thumbnail of it.
  * @return { Object } - Promise object.
  */
  return new Promise((resolve, reject) => {
    let img = new Image();

    img.onload = () => {
      resolve(obj)
    }

    img.onerror = () => {
      reject(new Error("Image hasn't been loaded properly. " + obj.thumb));
    }

    img.src = obj.thumb;
  })
}

module.exports = load_image;
