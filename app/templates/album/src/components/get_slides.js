import add_slide from "./add_slide";
import load_image from "./load_image";
import image_generator from "./image_generator";

let done = false;
const generator = image_generator(images);

function get_slides(actual_slide) {
  /**
  * Handles image generator. Adds slides with images to document with use of
  * add function.
  * @param { Number } - index of current slide
  */
  if (!done) {
    let promise_arr = [];

    for (let i = 0; i <= 3 != 0; i++) {
      let image_obj = generator.next();

      if (image_obj.done) {
        done = true;
        break;
      }

      promise_arr.push(load_image(image_obj.value))
    }

    Promise.all(promise_arr)
      .then(images => add_slide(images, actual_slide))
      .then(() => document.querySelector(".image-roll").style.transform = document.querySelector(".image-roll").style.transform = `translateX(-${100*actual_slide}%)`)
  }
}

module.exports = get_slides;
