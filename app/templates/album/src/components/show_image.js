function show_image(event) {
  /**
  * Shows image on image slider
  * @param { Object } event - JS event object
  */
  let slider = document.querySelector(".slider");
  slider.className += " visible loading-image";

  let img = document.querySelector("img");
  img.onload = () => slider.className = slider.className.replace(" loading-image", "");

  img.src = event.target.getAttribute("data-path");
}

module.exports = show_image;
