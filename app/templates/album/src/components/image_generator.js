function *image_generator(images) {
  /**
  * Generator function for images responsible for providing image objects.
  * @param { Object } images - array with image objects.
  */
  let i = 0;

  while(i < images.length)  {
    yield images[i++];
  }
}

module.exports = image_generator;
