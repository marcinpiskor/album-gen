import show_image from "./show_image";

function add_slide(images, index) {
  /**
  * Creates slide and adds image to it.
  * @param { Object } images - array with image objects of current slide.
  * @param { Number } index - index of current slide.
  */
  let slide_block = document.createElement('div');
  slide_block.className = "album-page normal";
  slide_block.setAttribute("data-id", index);

  let left_bar = document.createElement('div');
  left_bar.className = "leftbar";
  slide_block.appendChild(left_bar);

  let content = document.createElement('div');
  content.className = "content";
  slide_block.appendChild(content);

  let box = document.createElement('div');
  box.className = "box";
  content.appendChild(box);

  for (let image of images) {
    let snap_block = document.createElement('div');

    snap_block.className = "snap";
    snap_block.addEventListener("click", show_image);
    snap_block.setAttribute("data-path", image.normal);
    snap_block.style.backgroundImage = `url('${image.thumb}')`;

    box.appendChild(snap_block);
  }

  document.querySelector(".image-roll").appendChild(slide_block);
}

module.exports = add_slide;
