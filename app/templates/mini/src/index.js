import "babel-polyfill";
import globals from "./components/private_globals";
import handle_first_slide from "./components/handle_first_slide";
import {fill_circle, unfill_circle} from "./components/circle/modify_circle";
import describeArc from "./components/circle/describe_arc";
import get_images from "./components/get_images";
import {previous_slide, next_slide} from "./components/change_slide";

window.onload = () => {
  // INITIALIZING CHANGE SLIDE BUTTONS
  document.getElementById("left-arc").setAttribute("d", describeArc(25, 25, 24, 0, 359));
  document.getElementById("right-arc").setAttribute("d", describeArc(25, 25, 24, 0, 359));

  // HANDLING CHANGE SLIDE BUTTONS HOVER AND CLICK EVENTS
  let left_arr = {
    deg: 0,
    fill: null,
    unfill: null,
    el: document.querySelector("#left-arc-fill")
  };

  let right_arr = {
    deg: 0,
    fill: null,
    el: document.querySelector("#right-arc-fill")
  };

  let left_arrow_wrapper = document.querySelector("#left-arrow .svg-wrapper")

  left_arrow_wrapper.addEventListener("mouseenter", () => fill_circle(left_arr));
  left_arrow_wrapper.addEventListener("mouseleave", () => unfill_circle(left_arr));
  left_arrow_wrapper.addEventListener("click", () => previous_slide());

  let right_arrow_wrapper = document.querySelector("#right-arrow .svg-wrapper")

  right_arrow_wrapper.addEventListener("mouseenter", () => fill_circle(right_arr));
  right_arrow_wrapper.addEventListener("mouseleave", () => unfill_circle(right_arr));
  right_arrow_wrapper.addEventListener("click", () => next_slide());

  // TOUCH SUPPORT FOR SLIDE CHANGE
  let img = document.querySelector('img'), touch = {}, x_move;

  img.addEventListener("touchstart", event => touch = event, false);
  img.addEventListener("touchmove", event => {
    x_move = ((event.changedTouches[0].screenX - touch.touches[0].screenX)/window.innerWidth)*100;
    img.style.transform = `translateX(${x_move}%)`;
    img.style.opacity = 1 - Math.abs(x_move / 100);
  }, false);

  img.addEventListener("touchend", event => {
    if ((globals.actual_slide === 0 && x_move > 0) ||
        (globals.actual_slide === images.length - 1 && x_move < 0)) {
      handle_first_slide();
    }
    else if (x_move > 0) previous_slide(true);
    else if (x_move < 0) next_slide(true);
  }, false);

  // KEYBOARD ARROWS SUPPORT FOR SLIDE CHANGE
  document.querySelector("body").addEventListener("keydown", event => {
    switch(event.key) {
      case "ArrowLeft":
        previous_slide();
        break;
      case "ArrowRight":
        next_slide();
        break;
    };
  });

  get_images();
}
