function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
  /**
  * Prepares info necessary to create path of path element.
  * @param { Number } centerX - x coordinate of SVG element
  * @param { Number } centerY - y coordinate of SVG element
  * @param { Number } centerX - radius of circle
  * @param { Number } angleInDegrees - angle of circle in degrees
  * @param { Object } - object with info about path element
  */
  var angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;

  return {
    x: centerX + (radius * Math.cos(angleInRadians)),
    y: centerY + (radius * Math.sin(angleInRadians))
  };
}

module.exports = polarToCartesian;
