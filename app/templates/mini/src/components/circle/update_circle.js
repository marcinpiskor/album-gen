import describeArc from "./describe_arc";

function update_circle(el, deg) {
  /**
  * Updates 'd' attribute of passed path element.
  * @param { Element } el - slide button (path elementh with circle) to update
  * @param { Number } deg - current degree of circle
  */
  el.setAttribute("d", describeArc(25, 25, 24, 0, deg))
}

module.exports = update_circle;
