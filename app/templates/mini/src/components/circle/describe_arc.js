import polarToCartesian from "./polar_to_cartesian";

function describeArc(x, y, radius, startAngle, endAngle) {
  /**
  * Calculates d attribute of path element.
  * @param { Number } x - x coordinate of SVG element
  * @param { Number } y - y coordinate of SVG element
  * @param { Number } radius - radius of circle
  * @param { Number } startAngle - start angle of circle
  * @param { Number } endAngle - end angle of circle
  * @return { Number } - d attribute of particular path element
  */
  var start = polarToCartesian(x, y, radius, endAngle);
  var end = polarToCartesian(x, y, radius, startAngle);

  var largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";

  var d = [
      "M", start.x, start.y,
      "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y
  ].join(" ");

  return d;
}

module.exports = describeArc;
