import update_circle from "./update_circle";

function fill_circle(arr) {
  /**
  * Creates immersion of drawing (filling) circle by invoking function responsible
  * for updating 'd' attribute of path element in interval. If unfilling circle
  * is in process, that interval will be cleared.
  * @param { Object } arr - object with info about slide (arrow) button
  */
  if (arr.unfill) clearInterval(arr.unfill);

  arr.fill = setInterval(
  () => {
    if (arr.deg < 359) {
      arr.deg += 1;
      update_circle(arr.el, arr.deg);
    }
    else {
      clearInterval(arr.fill);
    }
  }, 1)
}

function unfill_circle(arr) {
  /**
  * Creates immersion of undrawing (unfilling) circle by invoking function responsible
  * for updating 'd' attribute of path element in interval. If filling circle
  * is in process, that interval will be cleared.
  * @param { Object } arr - object with info about slide (arrow) button
  */
  if (arr.fill) clearInterval(arr.fill);

  arr.unfill = setInterval(
    () => {
      if (arr.deg >= 0) {
      arr.deg -= 1;
      update_circle(arr.el, arr.deg);
    }
    else {
      clearInterval(arr.unfill);
    }
  }, 1)
}


module.exports = {fill_circle: fill_circle, unfill_circle: unfill_circle};
