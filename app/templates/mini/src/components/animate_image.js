function animate_image(move, opacity) {
  /**
  * Creates immersion of animating image by setting transition of opacity and
  * transform properties with delay, translating X coordinate of image and
  * setting opacity.
  * @param { Number } move - amount of percentages to translate X coordinate.
  * @param { Number } opacity - opacity of image;
  */
  let img = document.querySelector("img");

  img.style.transition = "opacity .4s, transform .4s";
  img.style.transform = `translateX(${move}%)`;
  img.style.opacity = String(opacity);
}

module.exports = animate_image;
