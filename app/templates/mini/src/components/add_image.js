import {open_slider} from "./open_slider";

function add_image(obj, index) {
  /**
  * Adds thumb block to thumn section and sets its background image and data-id
  * that corresponds to index of image object in array.
  * @param { Object } obj - image object that contain path to normal image and
  * thumbnail of it.
  * @param { Number } index - index of image object in array.
  */
  let thumb_block = document.createElement('div');

  thumb_block.className = "thumb";
  thumb_block.addEventListener("click", open_slider)
  thumb_block.style.backgroundImage = `url("${obj.thumb}")`;
  thumb_block.setAttribute('data-id', index);

  document.querySelector(".thumbs-wrapper").appendChild(thumb_block);
}

module.exports = add_image;
