import globals from "./private_globals";
import image_generator from "./image_generator";
import add_image from "./add_image";
import load_image from "./load_image";
import {set_slider} from "./open_slider";
import change_thumb from "./change_thumb";

let done = false;
const generator = image_generator(images);

function get_images() {
  /**
  * Handles image generator. Adds images to document with use of add function.
  */
  if (!done) {
    let promise_arr = [];

    for (let i = 1; i % 5 != 0; i++) {
      let image_obj = generator.next();

      if (image_obj.done) {
        done = true;
        break;
      }

      promise_arr.push(load_image(image_obj.value))
    }

    if (promise_arr.length > 0) {
      Promise.all(promise_arr)
        .then(images => images.forEach((image, i) => add_image(image, i + (globals.iter_count * 4))))
        .then(() => {
          (globals.iter_count === 0) ? set_slider(document.querySelector(".thumb")) : change_thumb("next");
          document.querySelector(".thumbs-wrapper").style.transform = `translateX(-${globals.iter_count * 102.5}%)`;
          globals.iter_count++;
        })
    }
  }
}

module.exports = get_images;
