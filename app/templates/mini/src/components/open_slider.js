import globals from "./private_globals";

let fig = document.querySelector("figure");

function open_slider(event) {
  /**
  * Fades out of image, changes index of actual image based on clicked thumb and
  * invokes function that updates it.
  * @param { Event } - JS click event
  */
  document.querySelector(".active").className = "thumb";
  globals.actual_slide = parseInt(event.target.getAttribute("data-id"));
  fig.style.opacity = "0";
  fig.childNodes[1].onload = () => fig.style.opacity = "1";

  setTimeout(() => set_slider(event.target), 400);
}

function set_slider(thumb) {
  /**
  * Updates src attribute of image based on clicked thumb and add to it active
  * class.
  * @param { Element } thumb - clicked thumb
  */
  fig.childNodes[1].src = images[globals.actual_slide].normal;
  thumb.className += " active";
}

module.exports = {open_slider: open_slider, set_slider: set_slider};
