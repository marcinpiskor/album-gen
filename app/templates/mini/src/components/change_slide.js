import globals from "./private_globals";
import get_images from "./get_images";
import change_thumb from "./change_thumb";
import {handle_touch, handle_normal} from "./handle_slider_events";
import {previous_thumbs, next_thumbs} from "./move_thumbs";

let img = document.querySelector("img");

function previous_slide(touch=false) {
  /**
  * Decrements index of actual image, invokes function responsible for changing
  * actual image thumb and setting slider image and optionally, calls function
  * for changing X position of thumbs wrapper to show thumbs of previous images.
  * @param { Boolean } touch - holds info if touch event has been emitted
  */
  if (globals.actual_slide - 1 >= 0 && img.className.length === 0) {
    img.onload = () => move_slide("prev-move");
    img.className = "in-process";

    change_thumb("prev");

    if (globals.actual_slide % 4 === 0) previous_thumbs();
    globals.actual_slide--;

    touch ? handle_touch(80) : handle_normal("fade-actual-prev");
  }
}

function next_slide(touch=false) {
  /**
  * Increments index of actual image, invokes functions responsible for changing
  * actual image thumb, loading next image thumbs, setting slider image and
  * optionally, calls function for changing X position of thumbs wrapper to show
  * thumbs of next images.
  * @param { Boolean } touch - holds info if touch event has been emitted
  */
  if (globals.actual_slide + 1 != images.length && img.className.length === 0) {
    img.onload = () => move_slide("next-move");
    img.className = "in-process";

    globals.actual_slide++;

    if (globals.actual_slide / 4 === globals.iter_count) get_images();
    else if (globals.actual_slide % 4 === 0) next_thumbs();
    else change_thumb("next");

    touch ? handle_touch(-80) : handle_normal("fade-actual-next");
  }
}

function move_slide(class_name) {
  /**
  * Adds class to image that stores info about animation of it.
  * @param { String } class_name - name of class with info about animation
  */
  img.className = class_name;

  setTimeout(() => {
    img.style.transform = "";
    img.style.opacity = "1";
    img.className = "";
  }, 600);
}

module.exports = {previous_slide: previous_slide, next_slide: next_slide};
