import globals from "./private_globals";
import change_thumb from "./change_thumb";

let thumbs_wrap = document.querySelector(".thumbs-wrapper");

function previous_thumbs() {
  /**
  * Changes horizontal position to show previous images thumbs.
  */
  let value = `translateX(-${((globals.actual_slide / 4) - 1) * 102.5}%)`;
  thumbs_wrap.style.transform = value;
}

function next_thumbs() {
  /**
  * Changes horizontal position to show next images thumbs and updates actual
  * thumb.
  */
  let value = `translateX(-${(globals.actual_slide / 4) * 102.5}%)`;
  thumbs_wrap.style.transform = value;
  change_thumb("next");
}

module.exports = {previous_thumbs: previous_thumbs, next_thumbs: next_thumbs};
