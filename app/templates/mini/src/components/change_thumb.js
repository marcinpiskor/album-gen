function change_thumb(move) {
  /**
  * Changes actual thumb based on move argument, to next or previous sibling of
  * actual thumb,
  * @param { String } move - string with info which change slide action has
  * occured ('next' or 'previous')
  */
  let thumb = document.querySelector(".active");
  let new_thumb = move === "next" ? thumb.nextSibling : thumb.previousSibling;
  thumb.className = "thumb";
  new_thumb.className += " active";
}

module.exports = change_thumb;
