import globals from "./private_globals";
import animate_image from "./animate_image";

let img = document.querySelector("img");

function handle_touch(move) {
  /**
  * Fades out of image with use of animate function (to left or right direction,
  * dependently on move argument) and updates src attribute of image.
  * @param { Number } move - decides if image will fade out to left or right (
  * to left - less than 0, to right - more than 0)
  */
  animate_image(move, 0);

  setTimeout(() =>  {
    img.style.transition = "";
    img.src = images[globals.actual_slide].normal;
  }, 600)
}

function handle_normal(class_name) {
  /**
  * Fades out of image by adding class name to it with info about animation and
  * updates src attribute of image.
  * @param { String } class_name - name of class that stores info about fade out
  * (left or or right) animation.
  */
  img.className = class_name;
  setTimeout(() => img.src = images[globals.actual_slide].normal, 600);
}

module.exports = {handle_touch: handle_touch, handle_normal: handle_normal}
