import animate_image from "./animate_image";

function handle_first_slide() {
  /**
  * Invokes function for animating image with arguments that will reset position
  * of image (e.g. current image is last, if user swipe left, without that
  * function, image will stay with opacity and X position based on last touch)
  */
  animate_image(0, 1);
  setTimeout(() => document.querySelector("img").style.transition = "", 400)
}

module.exports = handle_first_slide;
