import "babel-polyfill";
import get_slides from "./components/get_slides";
import scroll_load from "./components/scroll_load";
import change_layout_size from "./components/layout/change_layout_size";

window.onload = () => {
  get_slides(true);

  // Setting container margin from the header
  document.querySelector(".container").style.margin = `${document.querySelector('header').offsetHeight + 30}px 5%`;

  // Attach close event to slider button
  document.querySelector("button").addEventListener("click", () => {
    document.querySelector(".slider").className = "slider";
    setTimeout(() => document.querySelector(".slider img").className = "", 300);
  })

  window.onscroll = scroll_load;
  window.onresize = change_layout_size;
}
