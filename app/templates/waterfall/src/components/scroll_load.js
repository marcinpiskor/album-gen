import get_slides from "./get_slides";

function scroll_load() {
  /**
  * Invokes function responsible for loading images if user reaches bottom of
  * the page.
  */
  let scroll_height = Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
  let scroll_top = Math.max(document.body.scrollTop, document.documentElement.scrollTop);

  if (scroll_height - scroll_top === window.innerHeight) {
    get_slides();
  }
}

module.exports = scroll_load;
