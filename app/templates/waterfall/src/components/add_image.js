import open_slider from "./open_slider";
import get_column from "./layout/get_column";

function add_image(obj, index) {
  /**
  * Creates figure and adds image to it.
  * @param { Object } obj - object with normal image and thumb version of it.
  * @param { Number } index - index of current image.
  */
  let fig = document.createElement("figure");

  let img = new Image();
  img.src = obj.thumb;
  img.setAttribute("data-path", obj.normal)

  fig.appendChild(img);
  fig.addEventListener("click", open_slider);
  setTimeout(() => fig.className += "visible", index * 0.1);

  get_column().appendChild(fig);
}

module.exports = add_image;
