import get_view_status from "./layout/get_view_status";

let private_globals = {
  columns: Array.from(document.querySelectorAll(".column")),
  current_status: get_view_status()
}

module.exports = private_globals;
