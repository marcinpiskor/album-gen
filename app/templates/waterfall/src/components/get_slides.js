import image_generator from "./image_generator";
import load_image from "./load_image";
import add_image from "./add_image";

let done = false;
const generator = image_generator(images);

function check_if_init() {
  /**
  * Invokes function responsible for loading images until vertical bar is visible
  * @param { boolean } init - if page loading is still in process
  */
  let scroll_height = Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
  if (scroll_height === window.innerHeight) get_slides(true);
}

function get_slides(init=false) {
  /**
  * Handles image generator. Adds slides with images to document with use of
  * add function.
  * @param { boolean } init - if function has been invoked on page loading
  */
  if (!done) {
    let promise_arr = [];

    for (let i = 0; i < 8 != 0; i++) {
      let image_obj = generator.next();

      if (image_obj.done) {
        done = true;
        break;
      }

      promise_arr.push(load_image(image_obj.value))
    }

    Promise.all(promise_arr)
      .then(images => images.forEach((image, index) => add_image(image, index)))
      .then(() => { if (init) check_if_init() })
  }
}

module.exports = get_slides;
