import globals from "../private_globals";
import get_view_status from "./get_view_status";

function get_columns() {
  /**
  * Returns array with column elements dependently on view status.
  * @return { Object } - array with column elements.
  */
  return get_view_status() === "normal" ? globals.columns : globals.columns.slice(0, 2);
}

module.exports = get_columns;
