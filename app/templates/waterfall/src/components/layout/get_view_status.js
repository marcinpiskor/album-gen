function get_view_status() {
  /**
  * Returns view status based on window width.
  * @return { String } - view status
  */
  return window.innerWidth > 600 ? "normal" : "mobile";
}

module.exports = get_view_status;
