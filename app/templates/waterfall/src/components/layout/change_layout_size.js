import globals from "../private_globals";
import get_view_status from "./get_view_status";
import get_column from "./get_column";

function change_layout_size() {
  /**
  * Changes layout size if view status has been updated.
  */
  let new_status = get_view_status();

  if (globals.current_status != new_status) {
    globals.current_status = new_status;

    switch (new_status) {
      case "mobile":
        // Get images from two last columns and add them to two first columns
        globals.columns.slice(2,4).forEach((col) => {
          Array.from(col.childNodes).forEach(img => get_column().appendChild(img))
        })
        break;

      case "normal":
      // Get half of images from two first columns and add them to two last columns
        globals.columns.slice(0,2).forEach((col) => {
          let img_to_move = Array.from(col.childNodes).slice(parseInt(col.childNodes.length/2), col.childNodes.length);
          img_to_move.forEach(img => get_column().appendChild(img))
        });
    }
  }
}

module.exports = change_layout_size;
