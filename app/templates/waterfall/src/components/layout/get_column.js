import get_columns from "./get_columns";

function get_column() {
  /**
  * Returns shortest column element.
  * @return { DOM Element } - shortest column div
  */
  let active_columns = get_columns();
  let col_heights = active_columns.map(c => c.offsetHeight);

  return active_columns[col_heights.indexOf(Math.min(...col_heights))];
}

module.exports = get_column;
