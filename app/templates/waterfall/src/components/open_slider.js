function open_slider(event) {
  let spinner = document.querySelector(".spinner");
  spinner.style.visibility = "visible";
  spinner.className += " loading-process";

  let img = document.querySelector(".slider img");

  document.querySelector(".slider").className += " visible";
  img.onload = () => {
    img.className += "img-load";
    spinner.className = "spinner"
    setTimeout(() => { spinner.style.visibility = "hidden"; }, 200);
  }

  img.src = event.target.getAttribute("data-path");
}

module.exports = open_slider;
