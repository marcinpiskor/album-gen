from PyQt5 import QtWidgets
from .database import DatabaseThread


class MessageMixin:
    """
    Mixin with method that displays message.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def display_message(self, text, title="Błąd", stop=False):
        """
        Creates message box widget with passed info and displays it.

        Argument/s:
            text - message text
            title - message title
            stop - boolean if project creation has been stopped
        """
        if stop and self.progress_dial:
            self.progress_dial.close()

        message = QtWidgets.QMessageBox()
        message.setWindowTitle(title)
        message.setText(text)
        message.exec_()


def update_image(func):
    """
    Decorator for image widget buttons that updates image widget pixmap and
    emits changed signal.

    Argument/s:
        func - function to wrap
    """
    def wrapper(self):
        func(self)
        self.set_pixmap_image()
        self.parent().emitter.changed.emit()

    return wrapper

def check_project_validity(func):
    """
    Decorator for methods related with creating and saving project.
    Validates if title, description and images has been provided.

    Argument/s:
        func - function to wrap
    """
    def wrapper(self):
        if self.file_widget.proj_wrapper.validate_fields_data():
            if self.list_model.no_empty:
                func(self)
            else:
                self.display_message("Dodaj zdjęcia do albumu.")

        else:
            self.display_message("Podano nieprawidłowy tytuł i/lub opis.")

    return wrapper

def db_thread_wrapper(func):
    """
    Decorator for methods related with making operations on database.

    Argument/s:
        func - function to wrap
    """
    def wrapper(self):
        db_thread = DatabaseThread(parent=self, action=func)
        db_thread.run(self)

    return wrapper
