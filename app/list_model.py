from datetime import datetime
from PyQt5 import QtCore


class CustomListModelMixin:
    """Mixin that have customized list model methods."""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def rowCount(self, parent=QtCore.QModelIndex):
        return len(self.data)

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        if not index.isValid():
            return False

        if role == QtCore.Qt.EditRole:
            self.data[index.row()][self.data_name] = value
            self.dataChanged.emit(index, index)

        if role == QtCore.Qt.CheckStateRole:
            self.data[index.row()]["checked"] = not self.data[index.row()]["checked"]
            self.dataChanged.emit(index, index)

            return True

    def data(self, index, role=QtCore.Qt.EditRole):
        if not index.isValid():
            return QtCore.QVariant()

        if role == QtCore.Qt.CheckStateRole:
            if self.data[index.row()]["checked"]:
                return QtCore.QVariant(QtCore.Qt.Checked)
            else:
                return QtCore.QVariant(QtCore.Qt.Unchecked)

        if role == QtCore.Qt.DisplayRole:
            return self.get_display_role(index)

        return QtCore.QVariant()

    def get_display_role(self, index):
        if self.data_name == "name":
            return self.data[index.row()][self.data_name]
        else:
            return "{0} - {1}".format(self.data[index.row()][self.data_name],
                self.data[index.row()]["last_active"]
            )

    def flags(self, index):
        if index.isValid():
            return QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled

    def removeRows(self, position, parent=QtCore.QModelIndex()):
        self.beginRemoveRows(parent, position, position)
        data = self.data.pop(position)
        self.endRemoveRows()

        return data


class ImageListModel(CustomListModelMixin, QtCore.QAbstractListModel):
    """
    List model responsible for holding info about added images

    Attribute/s:
        data - list for list model
        loaded_project - dictionary with info about loaded project
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.data = []
        self.data_name = "name"
        self.loaded_project = None

    def insertRows(self, data, load=None, parent=QtCore.QModelIndex()):
        self.beginInsertRows(parent, self.rowCount(),
                                                self.rowCount() + len(data)-1)
        if load:
            for p in data:
                p["file_ext"] = p["path"].lower().split(".")[-1]
                self.handle_add_data(**p)

        else:
            self.handle_add_data(**data)

        self.endInsertRows()

    def handle_remove_data(self, actual):
        """
        Handles removing data by removing checked elements from data list.

        Returns boolean value if position of actual image of image preview
        widget has been changed or if it has been deleted.

        Argument/s:
            actual - index of actual image
        """
        removed_actual = False
        i = 0

        while i < self.rowCount():
            if self.data[i]["checked"]:
                if i <= actual:
                    removed_actual = True

                # If some project is loaded, check if checked image has been
                # already saved and related to project. If not, delete it from
                # list with images to save, else add id of that image to list
                # with images to delete.
                if self.loaded_project:
                    try:
                        self.loaded_project["added"].remove(self.data[i])
                    except ValueError:
                        self.loaded_project["removed"].append(
                                                        str(self.data[i]["id"]))

                self.removeRows(i)

            else:
                i += 1

        return removed_actual

    def handle_add_data(self, path, file_ext, file_id=None, checked=False):
        """
        Handles adding data by creating dictionary with name, path, checked
        status based on path and adding it to data list

        Argument/s:
            path - path to image file
            file_ext - extension of image file
        """
        new_image = {
            "path": path,
            "checked": checked,
            "name": path[:path.rfind(".")].split("/")[-1],
            "ext": file_ext,
            "id": file_id
        }

        self.data.append(new_image)

        # If some project has been loaded, add image to list with images to save
        if self.loaded_project:
            self.loaded_project["added"].append(new_image)

    def handle_select(self):
        """
        Selects or deselects element list based on amount of currently checked
        items
        """
        select_status = self.items_selected

        for index in range(len(self.data)):
            if self.data[index]["checked"] == select_status:
                model_index = self.index(index, 0, QtCore.QModelIndex())
                self.setData(model_index, "", role=QtCore.Qt.CheckStateRole)

    def update_data_id(self, updated_ids, mode):
        """
        Sets id of images after save or update the current loaded project

        Argument/s:
            updated_ids - list with id numbers of saved images
            mode - string with info if project has been created or updated
        """
        imgs_to_update = None

        if mode == 'save':
            imgs_to_update = self.data
        else:
            imgs_to_update = self.loaded_project["added"]

        for i in range(len(updated_ids)):
            imgs_to_update[i]["id"] = updated_ids[i][0]

    def load_project(self, project):
        """
        Invokes methods for load project and photos related to it.

        Argument/s:
            project - dictionary with info about project (title, description)
            and photos (paths to them)
        """
        self.clear()
        self.insertRows(project.pop("photos"), load=True)
        self.loaded_project = project
        self.clear_project()

    def clear(self):
        """
        Clears data list and actual project info
        """
        self.handle_select()
        self.handle_remove_data(0)
        self.loaded_project = None

    def changed(self, data):
        """
        Returns boolean if title or description of current project has been
        changed.

        Argument/s:
            data - dictionary with values of project line edits.
        """
        return (
                data["title"] != self.loaded_project["title"] or
                data["description"] != self.loaded_project["description"]
                )

    def clear_project(self):
        """
        Resets list that store info about changes of images in current project.
        """
        self.loaded_project["added"] = []
        self.loaded_project["removed"] = []

    @property
    def basic_info(self):
        """
        Returns dictionary with id, title, description and last active time of
        current project.
        """
        return {
                "title": self.loaded_project["title"],
                "id": self.loaded_project["id"],
                "last_active": self.loaded_project["last_active"],
                "checked": False
                }

    @property
    def updated(self):
        """
        Returns boolean that stores info if images of loaded project has been
        modified.
        """
        return (
                len(self.loaded_project["added"]) > 0 or
                len(self.loaded_project["removed"]) > 0
                )

    @property
    def no_empty(self):
        """
        Returns boolean if data list is not empty
        """
        return len(self.data) != 0

    @property
    def items_selected(self):
        """
        Returns boolean if all data list elements is checked
        """
        return all((item["checked"] for item in self.data))


class ProjectsListModel(CustomListModelMixin, QtCore.QAbstractListModel):
    """
    List model responsible for holding info about created projects

    Attribute/s:
        data - list for list model
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.data = self.parent().parent().database.load_projects()
        self.data_name = "title"

    def insertRows(self, data, parent=QtCore.QModelIndex()):
        self.beginInsertRows(parent, 0, 1)
        self.data.insert(0, data)
        self.endInsertRows()

    def handle_delete(self, cur_id):
        """
        Prepares string that contains id numbers of all checked projects to
        delete.
        """
        id_to_delete = ""
        i = 0

        while i < len(self.data):
            if self.data[i]["checked"] and cur_id != self.data[i]["id"]:
                id_to_delete += str(self.data[i]["id"]) + ", "
                self.removeRows(i)

            else:
                i += 1

        return id_to_delete[:len(id_to_delete) - 2]

    def handle_load(self):
        """
        Returns id number of first checked project on list.
        """
        try:
            return next((p["id"] for p in self.data if p["checked"]))
        except:
            return None

    def update_project_active(self, proj_id, proj_active):
        """
        Updates last active value of updated project and changes position of it
        on list.

        Argument/s:
            proj_id - id of updated project
            proj_active - new value of last active
        """
        ind = next((i for i in range(len(self.data)) if self.data[i]["id"] == proj_id))

        project = self.removeRows(ind)
        project["last_active"] = proj_active

        self.insertRows(project)
